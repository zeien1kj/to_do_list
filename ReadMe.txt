Kyle Zeien - 634608
CPS 396F
Central Michigan University

To Do List Features
- Uses JSON to store tasks for persistence
- Uses JSON to store pre-defined tasks
- Uses Firebase email authentication for allowing users to register and login
- Uses Firebase realtime database to track a users score
- Allows user to add a new task they need to complete
	- Can add a pre-defined task by clicking "Add Pre-Defined"
- Can add steps and remove steps freely so they can see a detailed view of what they need to do
- Can rename the task's title and modify steps to complete
- Can mark a task as complete or delete it
	- By marking a task as complete it will tracked in the users stats.json file and then displayed in the dashboard
	- If the user is logged in and they mark a task as complete, it will add points to their account and reflected on the leaderboard
- Users can pre-define repeated tasks so they do not need to type it out everytime
- Will show statistics of how many tasks they have completed/need to complete
- User can clear their stats to have a fresh start
- User can clear their current tasks list so they do not need to delete each one individually
- User can login and logout on settings page
- User can edit their pre-defined tasks from the settings page and add/delete/modify any tasks they want
- Has a leaderboard that shows a list of the top 10 users scores
- Scores are added by the formula of (2 x the number of subtasks plus a random number of 10-20)
