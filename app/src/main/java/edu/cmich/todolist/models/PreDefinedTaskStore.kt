package edu.cmich.todolist.models

interface PreDefinedTaskStore {
    fun findAll(): List<TaskModel>
    fun create(task: TaskModel)
    fun delete(task: TaskModel)
    fun update(task: TaskModel)
}