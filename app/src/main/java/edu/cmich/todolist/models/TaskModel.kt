package edu.cmich.todolist.models

import android.os.Parcelable
import android.widget.EditText
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class TaskModel(var id: Long = 0,
                     var title: String = "",
                     var childItems: MutableList<String> = arrayListOf()) : Parcelable