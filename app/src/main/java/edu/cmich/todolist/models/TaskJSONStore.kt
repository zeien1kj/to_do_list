package edu.cmich.todolist.models

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import edu.cmich.todolist.helpers.*
import edu.cmich.todolist.ui.StatModel
import org.jetbrains.anko.AnkoLogger
import java.util.*

val TASKS_FILE = "tasks.json"
val STATS_FILE = "stats.json"
val gsonBuilder = GsonBuilder().setPrettyPrinting().create()
val listType = object : TypeToken<java.util.ArrayList<TaskModel>>() {}.type

fun generateRandomId(): Long {
    return Random().nextLong()
}

class TaskJSONStore : TaskStore, AnkoLogger {

    val context: Context
    var tasks = mutableListOf<TaskModel>()

    constructor (context: Context) {
        this.context = context
        if (exists(context, TASKS_FILE)) {
            deserialize()
        }
    }

    override fun findAll(): MutableList<TaskModel> {
        return tasks
    }

    override fun create(task: TaskModel) {
        task.id = generateRandomId()
        tasks.add(task)
        serialize()
        //updateInProgress()
    }

    override fun delete(task: TaskModel){
        tasks.remove(task)
        serialize()
        //updateInProgress()
    }

    override fun complete(task: TaskModel){
        tasks.remove(task)
        serialize()
        updateStats(1)
        //updateInProgress()
    }

    override fun update(task: TaskModel) {
        var foundTask: TaskModel? = tasks.find { p -> p.id == task.id }
        if (foundTask != null) {
            foundTask.title = task.title
            foundTask.childItems = task.childItems
            serialize()
        }
    }

    private fun serialize() {
        val jsonString = gsonBuilder.toJson(tasks, listType)
        write(context, TASKS_FILE, jsonString)
    }

    private fun deserialize() {
        val jsonString = read(context, TASKS_FILE)
        tasks = Gson().fromJson(jsonString, listType)
    }

    private fun updateStats(modifier: Int){
        var jsonString = read(context, STATS_FILE)
        val stat = Gson().fromJson(jsonString, StatModel::class.java)
        stat.numCompleted = stat.numCompleted + modifier
        jsonString = Gson().toJson(stat)
        write(context, STATS_FILE, jsonString)
    }

//    private fun updateInProgress(){
//        var jsonString = read(context, STATS_FILE)
//        val stat = Gson().fromJson(jsonString, StatModel::class.java)
//        stat.numInProgress = tasks.size
//        jsonString = Gson().toJson(stat)
//        write(context, STATS_FILE, jsonString)
//    }

    override fun removeAllTasks(){
        tasks.clear()
        serialize()
    }

}