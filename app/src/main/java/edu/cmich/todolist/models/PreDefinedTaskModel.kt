package edu.cmich.todolist.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PreDefinedTaskModel (var id: Long = 0,
                                var title: String = "",
                                var childItems: MutableList<String> = arrayListOf()) : Parcelable