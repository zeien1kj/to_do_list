package edu.cmich.todolist.models

interface TaskStore {
    fun findAll(): List<TaskModel>
    fun create(task: TaskModel)
    fun update(task: TaskModel)
    fun delete(task: TaskModel)
    fun complete(task: TaskModel)
    fun removeAllTasks()

}