package edu.cmich.todolist.models

data class LeaderboardValue(var points: Int = 0, var username: String = "") {
}