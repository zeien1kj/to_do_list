package edu.cmich.todolist.models

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import edu.cmich.todolist.helpers.exists
import edu.cmich.todolist.helpers.read
import edu.cmich.todolist.helpers.write
import org.jetbrains.anko.AnkoLogger
import java.util.*

val PREDEFINED_FILE = "predefined.json"
val preDefined_listType = object : TypeToken<ArrayList<TaskModel>>() {}.type

class PreDefinedTaskJSONStore : PreDefinedTaskStore, AnkoLogger {

    val context: Context
    var preDefinedTasks = mutableListOf<TaskModel>()

    constructor (context: Context) {
        this.context = context
        if (exists(context, PREDEFINED_FILE)) {
            deserialize()
        }
    }

    override fun findAll(): MutableList<TaskModel> {
        return preDefinedTasks
    }

    private fun serialize() {
        val jsonString = gsonBuilder.toJson(preDefinedTasks, preDefined_listType)
        write(context, PREDEFINED_FILE, jsonString)
    }

    private fun deserialize() {
        val jsonString = read(context, PREDEFINED_FILE)
        preDefinedTasks = Gson().fromJson(jsonString, preDefined_listType)
    }

    override fun create(task: TaskModel){
        task.id = generateRandomId()
        preDefinedTasks.add(task)
        serialize()
    }

    override fun delete(task: TaskModel){
        preDefinedTasks.remove(task)
        serialize()
    }

    override fun update(task: TaskModel){
        var foundTask: TaskModel? = preDefinedTasks.find { p -> p.id == task.id }
        if (foundTask != null) {
            foundTask.id = generateRandomId()
            foundTask.title = task.title
            foundTask.childItems = task.childItems
            serialize()
        }
    }

}