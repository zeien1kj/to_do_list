package edu.cmich.todolist.ui.settings

import android.app.Activity
import android.app.AlertDialog
import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import edu.cmich.todolist.R
import edu.cmich.todolist.activities.LogInActivity
import edu.cmich.todolist.activities.PreDefinedListActivity
import edu.cmich.todolist.activities.TaskActivity
import edu.cmich.todolist.activities.TaskListActivity
import edu.cmich.todolist.helpers.read
import edu.cmich.todolist.helpers.write
import edu.cmich.todolist.main.MainApp
import edu.cmich.todolist.models.STATS_FILE
import edu.cmich.todolist.ui.StatModel
import kotlinx.android.synthetic.main.fragment_settings.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivityForResult


class SettingsFragment : Fragment(), AnkoLogger {

    private lateinit var settingsViewModel: SettingsViewModel
    lateinit var app: MainApp

    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        settingsViewModel =
            ViewModelProviders.of(this).get(SettingsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_settings, container, false)
        app = activity?.application as MainApp

        val btn_clearStats = root.findViewById<Button>(R.id.clearStats)
        val btn_clearTasks = root.findViewById<Button>(R.id.clearTasks)
        val btn_modifyPreTasks = root.findViewById<Button>(R.id.modifyPreTasks)

        var btn_Login = root.findViewById<Button>(R.id.loginBtn)

        btn_Login.setOnClickListener{
            if (app.loggedIn == false){
                activity?.let{
                    val intent = Intent(it, LogInActivity::class.java)
                    startActivityForResult(intent.putExtra("id", fbAuth.currentUser?.email), 13)
                }
            }
            else if (app.loggedIn == true){
                fbAuth.signOut()
                app.loggedIn = false
                loginBtn.text = "Login"
                info { "LOGGED OUT" }
            }

        }

        btn_clearStats.setOnClickListener{
            val builder = AlertDialog.Builder(activity)
            builder.setTitle("Clear Stats")
            builder.setMessage("Are you sure you want to clear your stats?")
            builder.setPositiveButton("YES"){dialog, which ->
                val stat = StatModel()
                stat.numCompleted = 0
                val jsonString = Gson().toJson(stat)
                write(app, "stats.json", jsonString)
                Toast.makeText(app, "Successfully Cleared Stats", Toast.LENGTH_SHORT).show()
            }
            builder.setNeutralButton("NO"){dialog, which ->

            }
            val dialog: AlertDialog = builder.create()

            dialog.show()
            //var jsonString = read(app, "stats.json")
            //val stat = Gson().fromJson(jsonString, StatModel::class.java)


        }

        btn_clearTasks.setOnClickListener{
            val builder = AlertDialog.Builder(activity)
            builder.setTitle("Clear Tasks")
            builder.setMessage("Are you sure you want to clear your tasks?")
            builder.setPositiveButton("YES"){dialog, which ->
                app.tasks.removeAllTasks()
                (activity as TaskListActivity).loadTasks()
                Toast.makeText(app, "Successfully Cleared Tasks", Toast.LENGTH_SHORT).show()
            }
            builder.setNeutralButton("NO"){dialog, which ->

            }
            val dialog: AlertDialog = builder.create()

            dialog.show()
        }

        btn_modifyPreTasks.setOnClickListener{
            activity?.let{
                val intent = Intent(it, PreDefinedListActivity::class.java)
                it.startActivityForResult(intent.putExtra("pre edit", ""), 0)
            }

        }
        //val textView: TextView = root.findViewById(R.id.textNotif)
        //settingsViewModel.text.observe(this, Observer {
        //    textView.text = it
        //})

        if (fbAuth.currentUser != null){
            btn_Login.text = "Logout"
            app.loggedIn = true
        }

        return root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 13){
            if (resultCode == Activity.RESULT_OK){
                info { "LOGGED IN" }
                loginBtn.text = "Logout"
                app.loggedIn = true
            }
        }
    }



}