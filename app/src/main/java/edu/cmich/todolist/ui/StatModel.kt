package edu.cmich.todolist.ui

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StatModel(var numCompleted: Int = 0): Parcelable