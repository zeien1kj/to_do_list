package edu.cmich.todolist.ui.dashboard

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import edu.cmich.todolist.R
import edu.cmich.todolist.main.MainApp
import org.jetbrains.anko.AnkoLogger
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import edu.cmich.todolist.helpers.read
import edu.cmich.todolist.helpers.write
import edu.cmich.todolist.models.LeaderboardValue
import edu.cmich.todolist.ui.StatModel
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.jetbrains.anko.info
import java.util.zip.Inflater

class DashboardFragment : AnkoLogger, Fragment() {

    lateinit var dashboardViewModel: DashboardViewModel
    lateinit var app: MainApp
    lateinit var progressText: TextView
    lateinit var completeText: TextView
    lateinit var subTasksText: TextView
    lateinit var stat: StatModel
    lateinit var root: View
    var fbDatabase = FirebaseDatabase.getInstance()
    var fbDatabaseReference = fbDatabase!!.reference!!.child("Users")
    var leaders: MutableList<LeaderboardValue> = arrayListOf()
    var usernames: MutableList<TextView> = arrayListOf()
    var points: MutableList<TextView> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        //val textView: TextView = root.findViewById(R.id.testDash)
        progressText = root.findViewById(R.id.numInProgress)
        completeText = root.findViewById(R.id.numCompleted)
        subTasksText = root.findViewById(R.id.numSubTasks)

        val btn_Refresh = root.findViewById<Button>(R.id.btn_Refresh)

        btn_Refresh.setOnClickListener{
            fillLeaderboard()
        }

        //val test: ConstraintLayout = root.findViewById(R.id.constraintLayout)
        //test.setBackgroundColor(Color.parseColor("#807C7C"))

        app = activity?.application as MainApp

        //val stat = StatModel(1, 0)
        //var gson = Gson()
        //val stat = StatModel(0, 0)
        //val jsonString = Gson().toJson(stat)
        //write(app, "stats.json", jsonString)

        //val jsonString = read(app, "stats.json")
        //var stat = StatModel(0, 0)
        //stat = Gson().fromJson(jsonString, StatModel::class.java)
        //task.numInProgress = 1
        //val jsonString2 = gson.toJson(task)
        //write(app, "stats.json", jsonString2)
        //info (stat)

        //dashboardViewModel.text.observe(this, Observer {
        //    textView.text = it
        //})

        fillLeaderboard()
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        usernames.add(username1)
        usernames.add(username2)
        usernames.add(username3)
        usernames.add(username4)
        usernames.add(username5)
        usernames.add(username6)
        usernames.add(username7)
        usernames.add(username8)
        usernames.add(username9)
        usernames.add(username10)

        points.add(score1)
        points.add(score2)
        points.add(score3)
        points.add(score4)
        points.add(score5)
        points.add(score6)
        points.add(score7)
        points.add(score8)
        points.add(score9)
        points.add(score10)
    }

    fun fillLeaderboard(){
        val pointListener = object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                leaders.clear()
                val children = p0!!.children
                info(children::class.java)
                children.forEach{
                    val tmp = it.getValue(LeaderboardValue::class.java)
                    info(tmp?.points)
                    leaders.add(tmp!!)
                }
                leaders.sortByDescending { it.points }

                var counter = 0
                for (leader in leaders){
                    if (counter >= 10){
                        info (counter)
                        break
                    }
                    info(leader.points)
                    info(leader.username)
                    points[counter].text = leader.points.toString()
                    usernames[counter].text = leader.username
                    counter++
                }
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        }

        fbDatabaseReference.addListenerForSingleValueEvent(pointListener)

    }

    override fun onHiddenChanged(hidden: Boolean){
        updateStats()
    }

    public fun updateStats(){
        val jsonString = read(app, "stats.json")
        stat = Gson().fromJson(jsonString, StatModel::class.java)
        progressText.text = "Number of Tasks to Complete: " + app.tasks.findAll().size
        completeText.text = "Number of Tasks Completed: " + stat.numCompleted.toString()
        var subTaskCounter: Int = 0
        for (i in app.tasks.findAll()){
            subTaskCounter += i.childItems.size
        }
        subTasksText.text = "Number of Sub-Tasks to Complete: " + subTaskCounter.toString()
    }
}