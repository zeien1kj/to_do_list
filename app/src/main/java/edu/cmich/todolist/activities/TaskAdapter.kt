package edu.cmich.todolist.activities

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import edu.cmich.todolist.R
import edu.cmich.todolist.models.TaskModel
import kotlinx.android.synthetic.main.task_placeholder.view.*
import java.lang.StringBuilder

interface TaskListener {
    fun onTaskClick(task: TaskModel)
}

class TaskAdapter constructor(private var tasks: List<TaskModel>,
                              private val listener: TaskListener) : RecyclerView.Adapter<TaskAdapter.MainHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainHolder {
        return MainHolder(LayoutInflater.from(parent?.context).inflate(R.layout.task_placeholder, parent, false))
    }

    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        val task = tasks[holder.adapterPosition]
        holder.bind(task, listener)
    }

    override fun getItemCount(): Int = tasks.size

    class MainHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bind(task: TaskModel, listener: TaskListener){
            itemView.taskTitle.text = task.title
            val sb = StringBuilder()
            var tracker: Int = 0
            for (value in task.childItems){
                ++tracker
                sb.append(" - ").append(value)
                if (tracker != task.childItems.size){
                    sb.append('\n')
                }
            }
            itemView.taskDescription.text = sb
            itemView.setOnClickListener{listener.onTaskClick(task)}
        }
    }
}