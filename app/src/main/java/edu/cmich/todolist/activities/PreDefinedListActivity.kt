package edu.cmich.todolist.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import edu.cmich.todolist.R
import edu.cmich.todolist.main.MainApp
import edu.cmich.todolist.models.TaskModel
import edu.cmich.todolist.models.generateRandomId
import kotlinx.android.synthetic.main.activity_pre_defined_list.*
import kotlinx.android.synthetic.main.activity_tasklist.*
import kotlinx.android.synthetic.main.activity_tasklist.toolbar
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivityForResult

class PreDefinedListActivity : AppCompatActivity(), AnkoLogger, TaskListener {

    lateinit var app: MainApp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pre_defined_list)
        app = application as MainApp
        toolbar.title = title
        setSupportActionBar(toolbar)

        val layoutManager = LinearLayoutManager(this)
        preDefineRecyclerView.layoutManager = layoutManager
        preDefineRecyclerView.adapter = TaskAdapter(app.preDefinedTasks.findAll(), this)
        loadTasks()
//        val temp = TaskModel()
//        temp.id = 298371237
//        temp.title = "This is a test Task"
//        temp.childItems.add("Test 1")
//        app.preDefinedTasks.create(temp)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val add = menu?.findItem(R.id.pre_item_add)
        add?.setVisible(true)
        val finish = menu?.findItem(R.id.item_cancel)
        finish?.title = "Finish"
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_task, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            R.id.item_cancel -> {
                finish()
            }
            R.id.pre_item_add -> {
                startActivityForResult(intentFor<TaskActivity>().putExtra("pre create",""), 0)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onTaskClick(task: TaskModel) {
        if (intent.hasExtra("pre edit")){
            startActivityForResult(intentFor<TaskActivity>().putExtra("pre edit", task), 0)
        }
        else {
            //task.id = generateRandomId()
            app.tasks.create(task.copy())
            finish()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        loadTasks()
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun loadTasks(){
        showTasks(app.preDefinedTasks.findAll())
    }

    fun showTasks(tasks: List<TaskModel>){
        preDefineRecyclerView.adapter = TaskAdapter(tasks, this)
        preDefineRecyclerView.adapter?.notifyDataSetChanged()
    }
}
