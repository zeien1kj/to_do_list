package edu.cmich.todolist.activities

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.core.Tag
import edu.cmich.todolist.R
import edu.cmich.todolist.main.MainApp
import edu.cmich.todolist.models.TaskModel
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.toast

class TaskActivity : AppCompatActivity(), AnkoLogger {

    var task = TaskModel()
    lateinit var app: MainApp
    var fbDatabase = FirebaseDatabase.getInstance()
    var fbDatabaseReference = fbDatabase!!.reference!!.child("Users")
    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbarAdd.title = title
        var edit = false
        setSupportActionBar(toolbarAdd)
        info("Task Activity Started...")
        info(fbAuth.currentUser?.uid)
        app = application as MainApp
        val myLinearLayout = findViewById<LinearLayout>(R.id.linearLayout)
        var stepCounter = 0

        if(intent.hasExtra("task edit")){
            edit = true
            task = intent.extras?.getParcelable<TaskModel>("task edit")!!
            taskTitle.setText(task.title)
            btnAdd.setText(R.string.save_task)

            for (i in task.childItems){
                val editText = LayoutInflater.from(this).inflate(R.layout.edit_text, null) as EditText
                myLinearLayout.addView(editText, 1 + stepCounter)
                editText.setText(i)
                stepCounter += 1
            }

            btnDelete.setOnClickListener(){
                app.tasks.delete(task.copy())
                setResult(RESULT_OK)
                finish()
            }

            btnDelete.visibility = View.VISIBLE
            btnDelete.isClickable = true

            btnComplete.setOnClickListener(){
                if (fbAuth.currentUser != null){
                    addPoints(task.childItems.count())
                }

                app.tasks.complete(task.copy())
                setResult(RESULT_OK)
                finish()
            }

            btnComplete.visibility = View.VISIBLE
            btnComplete.isClickable = true
        }

        if(intent.hasExtra("pre edit")){
            edit = true
            task = intent.extras?.getParcelable<TaskModel>("pre edit")!!
            taskTitle.setText(task.title)
            btnAdd.setText(R.string.save_task)

            for (i in task.childItems){
                val editText = LayoutInflater.from(this).inflate(R.layout.edit_text, null) as EditText
                myLinearLayout.addView(editText, 1 + stepCounter)
                editText.setText(i)
                stepCounter += 1
            }

            btnDelete.setOnClickListener(){
                app.preDefinedTasks.delete(task.copy())

                setResult(RESULT_OK)
                finish()
            }

            btnDelete.visibility = View.VISIBLE
            btnDelete.isClickable = true
        }

        //var addCounter = 0

        btnAddStep.setOnClickListener(){
            val editText = LayoutInflater.from(this).inflate(R.layout.edit_text, null) as EditText
            editText.setHint("Step " + (stepCounter + 1).toString())
            myLinearLayout.addView(editText, 1 + stepCounter)
            stepCounter += 1
        }


        btnAdd.setOnClickListener(){
            task.title = taskTitle.text.toString()
            task.childItems = arrayListOf()
            for (i in 1..myLinearLayout.childCount){
                if(myLinearLayout.getChildAt(i) is EditText){
                    val temp = myLinearLayout.getChildAt(i) as EditText
                    if(temp.text.toString() != ""){
                        task.childItems.add(temp.text.toString())
                    }

                }
            }

            if(task.title.isEmpty()){
                toast(R.string.enter_task_name)
            } else {
                if (edit){
                    if (intent.hasExtra("task edit")){
                        app.tasks.update(task.copy())
                    }
                    else if (intent.hasExtra("pre edit")) {
                        app.preDefinedTasks.update(task.copy())
                    }
                } else{
                    if (intent.hasExtra("pre create")){
                        app.preDefinedTasks.create(task.copy())
                    } else {
                        app.tasks.create((task.copy()))
                    }

                }
            }
            info("Add BTN pressed: $taskTitle")

            setResult(RESULT_OK)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_task, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            R.id.item_cancel -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun addPoints(subItems: Int){
        val currentUserDB = fbDatabaseReference!!.child(fbAuth.currentUser!!.uid)

        var points : String
        val pointListener = object : ValueEventListener{
            override fun onDataChange(p0: DataSnapshot) {
                val taskPoints = (subItems * 2) + (10..20).random()
                points = p0.value.toString()
                currentUserDB.child("points").setValue(points.toInt() + taskPoints)
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        }
        currentUserDB.child("points").addListenerForSingleValueEvent(pointListener)
    }
}
