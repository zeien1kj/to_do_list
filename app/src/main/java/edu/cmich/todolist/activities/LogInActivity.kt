package edu.cmich.todolist.activities

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import edu.cmich.todolist.R
import edu.cmich.todolist.main.MainApp
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.fragment_settings.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.toast
import kotlin.math.sign

class LogInActivity : AppCompatActivity(), AnkoLogger {


    var fbDatabase = FirebaseDatabase.getInstance()
    var fbDatabaseReference = fbDatabase!!.reference!!.child("Users")
    var fbAuth = FirebaseAuth.getInstance()
    //lateinit var app: MainApp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        //app = application as MainApp
        toolbarLogin.title = title
        setSupportActionBar(toolbarLogin)
        btnLogin.setOnClickListener{
            if(!text_email.text.isEmpty() && !text_password.text.isEmpty()){
                signIn(text_email.text.toString(), text_password.text.toString())
            }
            else {
                toast("Enter All Details")
            }

        }

        btnRegister.setOnClickListener {
            text_username.visibility = View.VISIBLE
            if(!text_email.text.isEmpty() && !text_password.text.isEmpty() && !text_username.text.isEmpty()){
                if (text_password.text.length > 7){
                    registerUser(text_email.text.toString(), text_password.text.toString(), text_username.text.toString())
                } else {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Error")
                    builder.setMessage("Password Must Be At Least 8 Characters Long")
                    builder.setPositiveButton("OK"){dialog, which ->
                    }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
            } else {
                toast("Fill In All Information")
            }
        }

    }

    fun signIn(email: String, password: String){
        fbAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, OnCompleteListener<AuthResult>{ task ->
            if(task.isSuccessful){
                setResult(RESULT_OK)
                finish()
            } else {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Error")
                builder.setMessage("Email or Password Incorrect")
                builder.setPositiveButton("OK"){dialog, which ->
                }
                val dialog: AlertDialog = builder.create()
                dialog.show()
                info{"FAILED"}
            }
        })
    }

    fun registerUser(email: String, password: String, username: String){
        fbAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this){ task ->
            if (task.isSuccessful){
                val userID = fbAuth!!.currentUser!!.uid
                val currentUserDB = fbDatabaseReference!!.child(userID)
                currentUserDB.child("username").setValue(username)
                currentUserDB.child("points").setValue(0)
                signIn(text_email.text.toString(), text_password.text.toString())
            }
            else{
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Error")
                builder.setMessage("Invalid E-mail Address")
                builder.setPositiveButton("OK"){dialog, which ->
                }
                val dialog: AlertDialog = builder.create()
                dialog.show()
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_task, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            R.id.item_cancel -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
