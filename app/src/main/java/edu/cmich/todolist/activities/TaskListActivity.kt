package edu.cmich.todolist.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isInvisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import edu.cmich.todolist.R
import edu.cmich.todolist.main.MainApp
import edu.cmich.todolist.models.TaskModel
import edu.cmich.todolist.ui.dashboard.DashboardFragment
import edu.cmich.todolist.ui.settings.SettingsFragment
import kotlinx.android.synthetic.main.activity_tasklist.*
import org.jetbrains.anko.*

class TaskListActivity : AppCompatActivity(), AnkoLogger, TaskListener {

    lateinit var app: MainApp

    val dashboardFragment = DashboardFragment()
    val settingsFragment = SettingsFragment()
    val fragmentManager = supportFragmentManager
    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_tasklist)
        app = application as MainApp
        toolbar.title = title
        setSupportActionBar(toolbar)

        val bottomNavigation: BottomNavigationView = findViewById(R.id.navView)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = TaskAdapter(app.tasks.findAll(), this)
        loadTasks()

        fragmentManager.beginTransaction().add(R.id.frame_layout, dashboardFragment, "1").hide(dashboardFragment).commit()
        fragmentManager.beginTransaction().add(R.id.frame_layout, settingsFragment, "2").hide(settingsFragment).commit()

    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId){
            R.id.navigation_home -> {
                fragmentManager.beginTransaction().hide(settingsFragment).hide(dashboardFragment).commit()
                recyclerView.isInvisible = false
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_overview -> {
                fragmentManager.beginTransaction().hide(settingsFragment).show(dashboardFragment).commit()
                recyclerView.isInvisible = true
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_settings -> {
                fragmentManager.beginTransaction().hide(dashboardFragment).show(settingsFragment).commit()
                recyclerView.isInvisible = true
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        loadTasks()
        dashboardFragment.updateStats()
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.item_add -> startActivityForResult<TaskActivity>(0)
            R.id.item_preDefined -> startActivityForResult<PreDefinedListActivity>(0)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onTaskClick(task: TaskModel) {
        startActivityForResult(intentFor<TaskActivity>().putExtra("task edit", task), 0)
    }

    fun loadTasks(){
        showTasks(app.tasks.findAll())
    }

    fun showTasks(tasks: List<TaskModel>){
        recyclerView.adapter = TaskAdapter(tasks, this)
        recyclerView.adapter?.notifyDataSetChanged()
    }

}