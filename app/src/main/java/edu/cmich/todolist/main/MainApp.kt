package edu.cmich.todolist.main

import android.app.Application
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import edu.cmich.todolist.models.PreDefinedTaskJSONStore
import edu.cmich.todolist.models.PreDefinedTaskStore
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import edu.cmich.todolist.models.TaskJSONStore
import edu.cmich.todolist.models.TaskStore


class MainApp : Application(), AnkoLogger {

    lateinit var tasks: TaskStore
    lateinit var preDefinedTasks: PreDefinedTaskStore
    var loggedIn: Boolean = false

    override fun onCreate() {
        super.onCreate()
        tasks = TaskJSONStore(applicationContext)
        preDefinedTasks = PreDefinedTaskJSONStore(applicationContext)

        info("App Started")
    }


}